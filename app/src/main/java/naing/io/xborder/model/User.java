/*
 * Copyright 2013 Google Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package naing.io.xborder.model;

import com.google.android.gms.maps.model.LatLng;
import com.google.maps.android.clustering.ClusterItem;

public class User implements ClusterItem {
    private String name="";
    public  int profilePhoto=0;
    private  LatLng mPosition=null;
    public  String fcmid="";

    private String email;
    private String password;
    private String created_at;
    private String newPassword;
    private String token;
    private String phone;
    private String credit;

    public User(){


    }
    public User(LatLng position, String name, String fcmid, String phone, int pictureResource) {
        this.name = name;
        profilePhoto = pictureResource;
        mPosition = position;
        this.phone = phone;
        this.fcmid = fcmid;
    }

    @Override
    public LatLng getPosition() {
        return mPosition;
    }

    @Override
    public String getTitle() {
        return this.name;
    }

    @Override
    public String getSnippet() {
        return null;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public void setCredit(String credit) {
        this.credit = credit;
    }

    public void setPhone(String phone) {this.phone = phone;}

    public void setName(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public String getEmail() {
        return email;
    }

    public String getCreated_at() {
        return created_at;
    }

    public String getPhone(){return  phone;}

    public void setNewPassword(String newPassword) {
        this.newPassword = newPassword;
    }

    public void setToken(String token) {
        this.token = token;
    }


    public String getCredit() {
        return credit;
    }

}
