package naing.io.xborder;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.design.widget.Snackbar;
import android.text.Html;
import android.text.Spanned;
import android.text.method.LinkMovementMethod;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import naing.io.xborder.fragments.ChangePasswordDialog;
import naing.io.xborder.model.Response;
import naing.io.xborder.model.User;
import naing.io.xborder.network.NetworkUtil;
import naing.io.xborder.utils.Constants;

import java.io.IOException;

import retrofit2.adapter.rxjava.HttpException;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;
import rx.subscriptions.CompositeSubscription;

public class ProfileActivity extends HorizontalNtbActivity implements ChangePasswordDialog.Listener {

    public static final String TAG = ProfileActivity.class.getSimpleName();

    private TextView mTvName;
    private TextView mTvCredit;
    private TextView mTvPhone;
    private TextView mTvEmail;
    private TextView mTvDate;
    private Button mBtChangePassword;
    private Button mBtLogout;

    //private ProgressBar mProgressbar;

    private SharedPreferences mSharedPreferences;
    private String mToken;
    private String mEmail;

    private CompositeSubscription mSubscriptions;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile);
        mSubscriptions = new CompositeSubscription();
        initViews();
        initSharedPreferences();
        loadProfile();
    }

    @Override
    protected void startDemo() {
        //
    }

    private void initViews() {

        mTvName = (TextView) findViewById(R.id.tv_name);
        mTvCredit = (TextView) findViewById(R.id.tv_credit);
        mTvPhone = (TextView) findViewById(R.id.tv_phone);
        mTvEmail = (TextView) findViewById(R.id.tv_email);
        mTvDate = (TextView) findViewById(R.id.tv_date);
        mBtChangePassword = (Button) findViewById(R.id.btn_change_password);
        mBtLogout = (Button) findViewById(R.id.btn_logout);
       // mProgressbar = (ProgressBar) findViewById(R.id.progress);

        mBtChangePassword.setOnClickListener(view -> showDialog());
        mBtLogout.setOnClickListener(view -> logout());
    }

    private void initSharedPreferences() {
        mSharedPreferences = PreferenceManager.getDefaultSharedPreferences(this);
        mToken = mSharedPreferences.getString(Constants.TOKEN, "");
        mEmail = mSharedPreferences.getString(Constants.EMAIL, "");
    }

    private void logout() {
        Constants.continueToUpdateLocation = false;
        //initSharedPreferences();
        try {
            Thread.sleep(1000); //delay for a second so that Shared Preferences are populated
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
//        Log.d("mToken", mToken);
//        Log.d("mToken", mEmail);
        mSubscriptions.add(NetworkUtil.getRetrofit(mToken).logout(mEmail)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe(this::handleResponse, this::handleError));

        finish();
        Intent i = new Intent(this, MainActivity.class);
        // set the new task and clear flags
        i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(i);

    }

    private void showDialog() {

        ChangePasswordDialog fragment = new ChangePasswordDialog();

        Bundle bundle = new Bundle();
        bundle.putString(Constants.EMAIL, mEmail);
        bundle.putString(Constants.TOKEN, mToken);
        fragment.setArguments(bundle);
        fragment.show(getFragmentManager(), ChangePasswordDialog.TAG);
    }

    private void loadProfile() {

        mSubscriptions.add(NetworkUtil.getRetrofit(mToken).getProfile(mEmail)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe(this::handleResponse, this::handleError));
    }


    private void handleResponse(User user) {

        //mProgressbar.setVisibility(View.GONE);
        mTvName.setText(user.getName());
        mTvPhone.setText(user.getPhone());
        if (Float.valueOf(user.getCredit()) > 0)
            mTvCredit.setText("Credit : "+ user.getCredit());
        else {
           Spanned Text = Html.fromHtml("Credit is "+user.getCredit()+" "+
                    "<a href='https://oo92.bitbucket.io/'>Please top up</a>");
            mTvCredit.setMovementMethod(LinkMovementMethod.getInstance());
            mTvCredit.setText(Text);
        }
//        Log.d("phone_",user.getPhone());
        mTvEmail.setText(user.getEmail());
        mTvDate.setText("Member since: "+user.getCreated_at());
    }

    private void handleError(Throwable error) {
        //mProgressbar.setVisibility(View.GONE);
        if (error instanceof HttpException) {
            Gson gson = new GsonBuilder().create();
            try {
                String errorBody = ((HttpException) error).response().errorBody().string();
                Response response = gson.fromJson(errorBody, Response.class);
                showSnackBarMessage(response.getMessage());
            } catch (IOException e) {
                e.printStackTrace();
            }
        } else {
            showSnackBarMessage("Network Error !");
        }
    }

    private void handleResponse(Response response) {}

    private void showSnackBarMessage(String message) {
        Snackbar.make(findViewById(R.id.activity_profile), message, Snackbar.LENGTH_SHORT).show();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        mSubscriptions.unsubscribe();
    }

    @Override
    public void onPasswordChanged() {
        showSnackBarMessage("Password Changed Successfully !");
    }
}

