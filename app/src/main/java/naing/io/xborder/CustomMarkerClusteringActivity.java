/*
 * Copyright 2013 Google Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package naing.io.xborder;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.location.Location;
import android.location.LocationManager;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.preference.PreferenceManager;
import android.provider.Settings;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.Toast;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.places.Place;
import com.google.android.gms.location.places.ui.PlaceAutocompleteFragment;
import com.google.android.gms.location.places.ui.PlaceSelectionListener;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.maps.android.clustering.Cluster;
import com.google.maps.android.clustering.ClusterManager;
import com.google.maps.android.clustering.view.DefaultClusterRenderer;
import com.google.maps.android.ui.IconGenerator;
import com.mongodb.BasicDBObjectBuilder;
import com.mongodb.DBObject;
import com.mongodb.MongoClient;
import com.mongodb.MongoClientOptions;
import com.mongodb.MongoClientURI;
import com.mongodb.MongoWriteException;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoCursor;
import com.mongodb.client.MongoDatabase;
import com.mongodb.client.model.UpdateOptions;

import org.bson.Document;
import org.bson.conversions.Bson;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;


import naing.io.xborder.model.User;
import naing.io.xborder.utils.Constants;
import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

import static com.mongodb.client.model.Filters.eq;


/**
 * Demonstrates heavy customisation of the look of rendered clusters.
 */
public class CustomMarkerClusteringActivity extends HorizontalNtbActivity implements ClusterManager.OnClusterClickListener<User>,
        ClusterManager.OnClusterInfoWindowClickListener<User>,
        ClusterManager.OnClusterItemClickListener<User>,
        ClusterManager.OnClusterItemInfoWindowClickListener<User>,
        GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener,LocationListener {

    private static final String SERVER_KEY =
            "AAAA3UB2GhM:APA91bHHI-pZZELb_8a4xMPrFV_fykgTPWnLUYbfNWohZ5Dx8BnsZVTU-k62WJZ_nwyYUSGCeM6YRZuUu719bY8lQalhuxBD1YopY4tipKkMIeD8FxCbaDNmgvfiAcHo_VIwnwuitTHx";

    private ClusterManager<User> mClusterManager;
    private GoogleApiClient mGoogleApiClient;

    MongoClientURI connectionString;
    private MongoClient mongoClient;
    MongoDatabase database;
    MongoCollection collection;
    MongoClientOptions options;

    private String fcmid = "null";
    private String whoami ="";
    public static final MediaType JSON
            = MediaType.parse("application/json; charset=utf-8");
    public static final String FCM_MESSAGE_URL = "https://fcm.googleapis.com/fcm/send";
    private PlaceAutocompleteFragment autocompleteFragment;
    private double[] coords = new double[2];




    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        fcmid = FirebaseInstanceId.getInstance().getToken();

        //this chunk executed when you press on the notification and the main activity loads
        if (getIntent().getExtras() != null) {

        }
        synchronized (mGoogleApiClient = new GoogleApiClient.Builder(this)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .addApi(LocationServices.API)
                .build()) {
            mGoogleApiClient.connect();
        }

//        if (android.os.Build.VERSION.SDK_INT > 9) {
//            StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
//            StrictMode.setThreadPolicy(policy);
//        }

        connectionString = new MongoClientURI("mongodb://xborder_access_client:xb0rd3r_x_clients@139.59.237.25:2777/?authSource=NaingTest&authMechanism=SCRAM-SHA-1",
                MongoClientOptions.builder().socketKeepAlive(true));

        mongoClient = new MongoClient(connectionString);

        database= mongoClient.getDatabase("NaingTest");
        collection = database.getCollection("users");

    }

    @Override
    public void onConnected(Bundle bundle) {
        //Log.d("onconnected", "pass");
        boolean permissionGranted = false;
        if (ActivityCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_FINE_LOCATION)
                != PackageManager.PERMISSION_GRANTED) {
            // Check Permissions Now
            ActivityCompat.requestPermissions(this,
                    new String[]{android.Manifest.permission.ACCESS_FINE_LOCATION}, 1);
        }
        else{
            permissionGranted = true;
        }

        if(permissionGranted) {
            LocationRequest mLocationRequest = createLocationRequest();
           // Log.d("mlocation", mLocationRequest.toString());
            LocationServices.FusedLocationApi.requestLocationUpdates(
                    mGoogleApiClient, mLocationRequest, this);
        }
        else{
            Toast.makeText(getApplicationContext(), "Location cannot be retrieved", Toast.LENGTH_SHORT).show();
        }

    }

    private LocationRequest createLocationRequest() {
        LocationRequest mLocationRequest = new LocationRequest();
        mLocationRequest.setInterval(2000); //every 10 seconds
        mLocationRequest.setFastestInterval(1000);
        mLocationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
        return mLocationRequest;
    }

    @Override
    public void onConnectionSuspended(int i) {
        //Log.d("connection_suspended", "Connection to Google API suspended");
    }

    @Override
    public void onConnectionFailed( ConnectionResult connectionResult) {
       // Log.d("connection_suspended", "Connection to Google API failed");
    }

    @Override
    protected void onResume() {
        LocationManager lm = (LocationManager)this.getSystemService(Context.LOCATION_SERVICE);
        boolean gps_enabled = false;
        boolean network_enabled = false;


        try {
            gps_enabled = lm.isProviderEnabled(LocationManager.GPS_PROVIDER);
        } catch(Exception ex) {}

        try {
            network_enabled = lm.isProviderEnabled(LocationManager.NETWORK_PROVIDER);
        } catch(Exception ex) {}

        if(!gps_enabled && !network_enabled) {
            // notify user
            AlertDialog.Builder dialog = new AlertDialog.Builder(this);
            dialog.setMessage(this.getResources().getString(R.string.gps_network_not_enabled));
            dialog.setPositiveButton(this.getResources().getString(R.string.open_location_settings), new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface paramDialogInterface, int paramInt) {

                    Intent myIntent = new Intent( Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                    startActivity(myIntent);
                }
            });
            dialog.setNegativeButton(this.getString(R.string.Cancel), new DialogInterface.OnClickListener() {

                @Override
                public void onClick(DialogInterface paramDialogInterface, int paramInt) {
                    android.os.Process.killProcess(android.os.Process.myPid());

                }
            });
            dialog.show();
        }
        super.onResume();
    }

    @Override
    public void onBackPressed() {
        Toast.makeText(getApplicationContext(),"Please exit by logging out", Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onClusterInfoWindowClick(Cluster<User> cluster) {

    }

    /**
     * Draws profile photos inside markers (using IconGenerator).
     * When there are multiple people in the cluster, draw multiple photos (using MultiDrawable).
     */
    private class PersonRenderer extends DefaultClusterRenderer<User> {
        private final IconGenerator mIconGenerator = new IconGenerator(getApplicationContext());
        private final IconGenerator mClusterIconGenerator = new IconGenerator(getApplicationContext());
        private final ImageView mImageView;
        //        private final ImageView mClusterImageView;
        private final int mDimension;

        public PersonRenderer() {
            super(getApplicationContext(), getMap(), mClusterManager);

            View multiProfile = getLayoutInflater().inflate(R.layout.multi_profile, null);

            mClusterIconGenerator.setContentView(multiProfile);
            mImageView = new ImageView(getApplicationContext());
            mDimension = (int) getResources().getDimension(R.dimen.custom_profile_image);
            mImageView.setLayoutParams(new ViewGroup.LayoutParams(mDimension, mDimension));
            int padding = (int) getResources().getDimension(R.dimen.custom_profile_padding);
            mImageView.setPadding(padding, padding, padding, padding);
            mIconGenerator.setContentView(mImageView);

        }

        @Override
        protected void onBeforeClusterItemRendered(User person, MarkerOptions markerOptions) {
            // Draw a single person.
            // Set the info window to show their name.
            mImageView.setImageResource(person.profilePhoto);
            Bitmap icon = mIconGenerator.makeIcon();
            markerOptions.icon(BitmapDescriptorFactory.fromBitmap(icon)).title(person.getName());
            markerOptions.visible(true);

        }


        @Override
        protected void onBeforeClusterRendered(Cluster<User> cluster, MarkerOptions markerOptions) {

        }

        @Override
        protected boolean shouldRenderAsCluster(Cluster cluster) {
            // Always render clusters.
            //return cluster.getSize() > 1;
            return false;
        }
    }

    @Override
    public boolean onClusterClick(Cluster<User> cluster) {
        return true;
    }

    @Override
    public void onLocationChanged(Location location) {
        broadcastLocation(location);
    }

    private void broadcastLocation(final Location location) {
        SharedPreferences settings = PreferenceManager.getDefaultSharedPreferences(this);

        /* Geo spatial data needs to be of the type as here
            https://docs.mongodb.com/manual/geospatial-queries/ */

        //this self location update will keep running!
        Thread t1 = new Thread() {
            public void run() {
                if (Constants.continueToUpdateLocation) {
                    try {

                        //collection.find(new Document(Document.parse("{'email':{ '$eq': 'o@o.o'}},{'name':'1','_id':'0'}")));
                        whoami = settings.getString("my_name", "");
                        //TODO - resolve the bug here
                        collection.updateOne(eq("email", settings.getString("my_email", "")),
                                new Document("$set", Document.parse(
                                        //"{'name':" + "'" +
                                        //settings.getString("my_name", "") + "'"
                                        "{'location': { 'type': 'Point', coordinates: [ "
                                        + location.getLongitude()
                                        + ","
                                        + location.getLatitude()
                                        + "] }"
                                        + ", 'isOnline':"
                                        + 1
                                        + ", 'fcmid':"
                                        + "'" +
                                        fcmid + "'" + "}")),
                                new UpdateOptions().upsert(true)

                        );
                    } catch (MongoWriteException e) {
                        Log.e("Mongo Write Error", e.getMessage());
                    }
                }
            }
        };
        t1.start();

        // Get a handler that can be used to post to the main thread
        Handler mainHandler = new Handler(this.getMainLooper());

        Runnable myRunnable = new Runnable() {
            @Override
            public void run() {

                long distance = 5000; //in meters
                DBObject query = BasicDBObjectBuilder.start().push("location")
                        .push("$near").add("$maxDistance", distance).push("$geometry")
                        .add("type", "Point").add("coordinates", coords).get();

                new Thread() {
                    @Override
                    public void run() {
                        //your code here
                        MongoCursor <Document> cursor = collection.find((Bson) query).iterator();
                        Document docUserLocation;
                        List <User> personList = new ArrayList <User> ();

                        try {
                            while (cursor.hasNext()) {
                                docUserLocation = cursor.next();
                                //querying docUserLocation returns a nested location document from which we extract the actual coordinates
                                Document userLocation = (Document) docUserLocation.get("location");
                                //userLocation.get("coordinates") is returning a list of double
                                List <Double> coordinates = (List <Double> ) userLocation.get("coordinates");
                                if ((int) docUserLocation.get("isOnline") == 1) {
                                    if (!docUserLocation.get("name").toString().equals(whoami)) {
                                        personList.add(new User(new LatLng(coordinates.get(1), coordinates.get(0)),
                                                                            docUserLocation.get("name").toString(),
                                                                            docUserLocation.get("fcmid").toString(),
                                                                            docUserLocation.get("phone").toString()
                                                                            , R.drawable.noti));
                                    }
                                   // Log.d("personlist", personList.toString());
                                }
                            }
                            mClusterManager.clearItems();
                            for (User p: personList) {
                                mClusterManager.addItem(p);

                                //mClusterManager.addItem(p);
                            }
                        } finally {
                            cursor.close();
                        }

                    }
                }.start();
                mClusterManager.cluster();
            }
        };
        mainHandler.post(myRunnable);

    }

    @Override
    public boolean onClusterItemClick(User item) {
        // Does nothing, but you could go into the user's profile page, for example.
        SharedPreferences settings = PreferenceManager.getDefaultSharedPreferences(this);

        //Put up the Yes/No message box
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder
                .setTitle("Starting a chat with "+ item.getName()+".")
                .setMessage("Continue? Note: Your number will be shared with this person.")
                .setIcon(R.drawable.noti)
                .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        //Yes button clicked, create a room for chatting
                        sendMessage(item.fcmid, "xBorder: Incoming interest", whoami+"@"+settings.getString("my_phone",""), R.drawable.noti, whoami+"@"+settings.getString("my_phone",""));
                        //Log.d("fcmid", item.fcmid);

                        //option of giving a call here
                        Intent intent = new Intent(Intent.ACTION_DIAL);
                        //Log.d("phone_number", item.getPhone().toString());
                        intent.setData(Uri.parse("tel:"+item.getPhone().toString()));
                        startActivity(intent);
                        getIntent().removeExtra("STRING_I_NEED");


                    }
                })
                .setNegativeButton("No", null)	//Do Nothing
                .show();

        return false;

    }

    OkHttpClient mClient = new OkHttpClient();
    public void sendMessage(final String recipient, final String title, final String body, final int icon, final String message) {

        new AsyncTask<String, String, String>() {
            @Override
            protected String doInBackground(String... params) {
                try {
                    JSONObject root = new JSONObject();
                    JSONObject notification = new JSONObject();
                    notification.put("body", body);
                    notification.put("title", title);
                    notification.put("icon", icon);
                    notification.put("sound", "default");
                    JSONObject data = new JSONObject();
                    data.put("message", message);
                    root.put("notification", notification);
                    root.put("data", data);
                    root.put("to",recipient);
                    String result = postToFCM(root.toString());
                    //Log.d("RESULT", "Result: " + result);
                    return result;
                } catch (Exception ex) {
                    ex.printStackTrace();
                }
                return null;
            }

            @Override
            protected void onPostExecute(String result) {
                try {
                    JSONObject resultJson = new JSONObject(result);
                    int success, failure;
                    Log.e("fcm", resultJson.toString());
                    success = resultJson.getInt("success");
                    failure = resultJson.getInt("failure");
                    Toast.makeText(getApplicationContext(), "Message Success: " + success + "Message Failed: " + failure, Toast.LENGTH_LONG).show();
                } catch (JSONException e) {
                    e.printStackTrace();
                    Log.e("fcm", e.getMessage());
                    Toast.makeText(getApplicationContext(), "Message Failed, Unknown error occurred.", Toast.LENGTH_LONG).show();
                }
            }
        }.execute();
    }

    private String postToFCM(String bodyString) throws IOException {
        RequestBody body = RequestBody.create(JSON, bodyString);
        Request request = new Request.Builder()
                .url(FCM_MESSAGE_URL)
                .post(body)
                .addHeader("Authorization", "key=" + SERVER_KEY)
                .build();
        Response response = mClient.newCall(request).execute();
        return response.body().string();
    }

    @Override
    public void onClusterItemInfoWindowClick(User item) {
        // Does nothing, but you could go into the user's profile page, for example.
    }

    @Override
    protected void startDemo() {
        mClusterManager = new ClusterManager<User>(this, getMap());
        mClusterManager.setRenderer(new PersonRenderer());
        getMap().setOnCameraIdleListener(mClusterManager);
        getMap().setOnMarkerClickListener(mClusterManager);
        getMap().setOnInfoWindowClickListener(mClusterManager);
        mClusterManager.setOnClusterClickListener(this);
        mClusterManager.setOnClusterInfoWindowClickListener(this);
        mClusterManager.setOnClusterItemClickListener(this);
        mClusterManager.setOnClusterItemInfoWindowClickListener(this);

        autocompleteFragment = (PlaceAutocompleteFragment)getFragmentManager().findFragmentById(R.id.place_autocomplete_fragment);

        autocompleteFragment.setOnPlaceSelectedListener(new PlaceSelectionListener() {
            @Override
            public void onPlaceSelected(Place place) {
                coords[0] = place.getLatLng().longitude;
                coords[1] = place.getLatLng().latitude;//zoom into the currently searched location
                getMap().moveCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(coords[1], coords[0]), 15.5f));
            }

            @Override
            public void onError(Status status) {
                Log.i("Place_selected_", "An error occurred: " + status);
            }
        });

        mClusterManager.cluster();
    }

}
