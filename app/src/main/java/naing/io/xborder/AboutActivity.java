package naing.io.xborder;

import android.os.Bundle;
import android.webkit.WebView;



public class AboutActivity extends HorizontalNtbActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_about);
        WebView wv = (WebView) findViewById(R.id.webview);
        wv.getSettings().setBuiltInZoomControls(false);
        wv.loadUrl("file:///android_asset/html/index.html");
    }

    @Override
    protected void startDemo() {

    }
}
